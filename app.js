const Express = require('express');
const os = require('os');
const path = require('path');
const normalizePort = require('normalize-port')
const app = new Express();
const dev = process.env.NODE_ENV !== 'production'
const nextApp = require('next')({dev}); 
console.log('nextApp: ', nextApp);


const siteManifest = require(path.join(__dirname,'.next','build-manifest.json'));
console.log('siteManifest: ', siteManifest);

const devmode = true;

const hostname = devmode ? 'localhost' : os.networkInterfaces().en0.filter(networkDevice=> networkDevice.family==='IPv4')[0].address
const port = normalizePort(process.env.PORT || 3000);

app.use('/',Express.static(path.join(__dirname,'out')));
app.use('/about',Express.static(path.join(__dirname,'out/about')));

app.get('/',(req,res)=>{
    
    console.log('req.headers: ', req.headers);
    
    console.log('req.query: ', req.query);
    
    res.status(200).send('ok');
    res.end();
});

app.get('/about',(req,res)=>{
    console.log('req.headers: ', req.headers);
    
    console.log('req.query: ', req.query);
})

app.listen(port,hostname,()=>console.log(`|---${hostname}:${port}---|`))